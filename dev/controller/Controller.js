var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var Marionette = Backbone.Marionette = require('backbone.marionette');
var ScaffoldingLayoutView = require('./../view/ScaffoldingLayoutView');
var PersonLayoutView = require('./../view/PersonLayoutView');
var RegCompositeView = require('./../view/RegCompositeView');
var AboutTopCollectionView = require('./../view/AboutTopCollectionView');
var AboutLastCollectionView = require('./../view/AboutLastCollectionView');
var PersonAboutCollectionView = require('./../view/PersonAboutCollectionView');
var PersonTopCollectionView = require('./../view/PersonTopCollectionView');
var PersonItemView = require('./../view/PersonItemView');
var PersonModel = require('./../model/PersonModel');
var ProjectView = require('./../view/ProjectView');
var RuleView = require('./../view/RuleView');
var PersonFreandCollectionView = require('./../view/PersonFreandCollectionView');
var PersonBarItemView = require('./../view/PersonBarItemView');
var ContactItemView = require('./../view/ContactItemView');
var PassView = require('./../view/PassView');
var PublishCompositeView = require('./../view/PublishCollectionView');
var HeaderView = require('./../view/HeaderItemView');
var ModalAuthView = require('./../view/ModalAuthView');
var ConfirmItemView = require('./../view/ConfirmItemView');

console.log('Controller init');

module.exports = Backbone.Marionette.Controller.extend({
  scaffoldingLayoutView: ScaffoldingLayoutView,
  initialize: function() {

    var self = this;
    //this.scaffoldingLayoutView = ;
   // window.App.getRegion('scaffoldingRegion').show(this.scaffoldingLayoutView, { forceShow: true });
    //this.scaffoldingLayoutView.render();

  },
  reg: function() {
    this.setup();
    var self = this;
    var regCompositeView = new RegCompositeView();
    this.scaffoldingLayoutView.showChildView('appRegion', regCompositeView);
  },
  aboutTop: function() {
    this.setup();
    var aboutTopCollectionView = new AboutTopCollectionView();
    this.scaffoldingLayoutView.showChildView('appRegion', aboutTopCollectionView);
  },
  personTop: function() {
    this.setup();
    var personTopCollectionView = new PersonTopCollectionView();
    this.scaffoldingLayoutView.showChildView('appRegion', personTopCollectionView);
  },
  person: function(id) {
   /* var personLayoutView = new PersonLayoutView();
    console.log(personLayoutView);
    window.App.getRegion('scaffoldingRegion').show(personLayoutView, {
      forceShow: true
    });*/
    //var model = new PersonModel({us_id: this.us_id});
    window.App.getRegion('scaffoldingRegion').show(PersonLayoutView, { preventDestroy: true });
    PersonLayoutView.render();
    var personItemView = new PersonItemView({us_id: id});
    //var personItemView = new PersonItemView({model: model});
    //var headerView = new HeaderView();
    var personBarItemView = new PersonBarItemView({us_id: id});
    var personAboutCollectionView = new PersonAboutCollectionView({us_id: id});
   // PersonLayoutView.showChildView('appHeader', headerView)
    PersonLayoutView.showChildView('profileContent', personItemView);
    PersonLayoutView.showChildView('barContent', personBarItemView);
    PersonLayoutView.showChildView('commentContent', personAboutCollectionView);
    Backbone.Events.on('popupHeader', function() {
      var modalAuth = new ModalAuthView();
      PersonLayoutView.showChildView('appModal', modalAuth);
    });
  },
  personFreand: function(id) {
    console.log('PersonFreand route', id);
    this.setup();
    var personFreandCollectionView = new PersonFreandCollectionView({us_id: id});
    this.scaffoldingLayoutView.showChildView('appRegion', personFreandCollectionView);
  },
  aboutLast: function() {
    this.setup();
    var aboutLastCollectionView = new AboutLastCollectionView();
    this.scaffoldingLayoutView.showChildView('appRegion', aboutLastCollectionView);
  },
  publish: function() {
    this.setup();
    var publishCompositeView = new PublishCompositeView();
    this.scaffoldingLayoutView.showChildView('appRegion', publishCompositeView);
  },
  contact: function() {
    this.setup();
    var contactItemView = new ContactItemView();
    this.scaffoldingLayoutView.showChildView('appRegion', contactItemView);
  },
  project: function() {
    this.setup();
    var projectView = new ProjectView();
    this.scaffoldingLayoutView.showChildView('appRegion', projectView);
  },
  confirm: function(id) {
    this.setup();
    var confirmItemView = new ConfirmItemView({token: id});
    this.scaffoldingLayoutView.showChildView('appRegion', confirmItemView);
  },
  resetPass: function(id) {
    this.setup();
    var passView = new PassView({token: id});
    this.scaffoldingLayoutView.showChildView('appRegion', passView);
  },
  rule: function() {
    this.setup();
    var ruleView = new RuleView();
    this.scaffoldingLayoutView.showChildView('appRegion', ruleView);
  },
  setup: function() {

    var self = this;
    window.App.getRegion('scaffoldingRegion').show(this.scaffoldingLayoutView, { forceShow: true, preventDestroy: true });
    this.scaffoldingLayoutView.render();
    Backbone.Events.on('popupHeader', function() {
      var modalAuth = new ModalAuthView();
      self.scaffoldingLayoutView.showChildView('appModal', modalAuth);
    });
  }
});

