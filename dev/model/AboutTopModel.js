var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
//var moment = require('moment-timezone');
var moment = require('moment');
//var jstz = require('jstimezonedetect');


module.exports = Backbone.Model.extend({
  defaults: {
    authorId: null,
    commentCount: null,
    createdAt: null,
    disproofCount: null,
    id: null,
    isYours: null,
    person: null,
    proofCount: null,
    rating: null,
    status: "",
    text: "",
    data: [],
    place: ""
  },
  parse: function(resp) {

    this.date(resp);
    return resp;
  },
  date: function(resp) {

    console.log(resp.createdAt)
      resp.createdAt = new Date(resp.createdAt);
      resp.createdAt = moment(resp.createdAt).fromNow();
    //console.log(mDate);
      //resp.createdAt =
      return resp.createdAt;
  }
});
