var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;

module.exports = Backbone.Model.extend({
  defaults: {
    avatarUrl: "",
    birthday: "",
    city: "",
    country: "",
    externalId: "",
    firstName: "",
    iconUrl: "",
    id: null,
    isSubscribed: null,
    lastName: "",
    nickName: null,
    photoUrl: "",
    resource: "",
    screenName: "",
    vkId: "",
    place: "",
    stats: {
      comments: 0,
      effectiveVotes: 0,
      feedbacks: 0,
      feedbacksReputation: 0,
      reputation: 0,
      subscriptions: 0
    },
    friendsCount: 0
  }
});
