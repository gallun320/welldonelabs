var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var PersonTopModel = require('./PersonTopModel');

module.exports = PersonTopModel.extend({
  us_id: null,
  initialize: function(options) {
    this.us_id = options.us_id;
    var self = this;
    console.log(this);

    this.fetch({
      success: function() {
        window.idUser = self.get('id');
        var vkId = self.get('externalId');
        var monthSearch = self.get('birthday').indexOf('.');
        var month = self.get('birthday').slice(monthSearch + 1);
        var day = self.get('birthday').slice(0, monthSearch);
        var monthData = self.month(month);
        vkId = vkId.slice(2);
        self.set('vkId', vkId);
        if(monthData !== undefined) {
          self.set('birthday', day + ' ' + monthData);
        }


      }
    });
  },
  month: function(month) {
    switch(month) {
      case "1":
        return "января";
        break;
      case "2":
        return "февраля";
        break;
      case "3":
        return "марта";
        break;
      case "4":
        return "апреля";
        break;
      case "5":
        return "майя";
        break;
      case "6":
        return "июня";
        break;
      case "7":
        return "июля";
        break;
      case "8":
        return "августа";
        break;
      case "9":
        return "сентября";
        break;
      case "10":
        return "октября";
        break;
      case "11":
        return "ноября";
        break;
      case "12":
        return "декабря";
        break;
      default:
        break;
    }
  },
  url: function() {
    return '/api/v1/persons/' + this.us_id + '/VK';
  },
  parse: function(resp) {
    return resp.response.person;
  }

});
