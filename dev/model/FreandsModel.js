var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;


module.exports = Backbone.Model.extend({
  defaults: {
    authorId: null,
    commentCount: null,
    createdAt: null,
    disproofCount: null,
    id: null,
    isYours: null,
    person: null,
    proofCount: null,
    rating: null,
    status: "",
    text: ""
  }
});
