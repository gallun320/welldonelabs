var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;

module.exports = Backbone.Model.extend({
  defaults: {
    authorId: null,
    authorIdTmp: null,
    createdAt: null,
    id: null,
    status: "",
    text: ""
  }
});
