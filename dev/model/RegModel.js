var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;

module.exports = Backbone.Model.extend({
  initialize: function() {
    console.log('RegModel init');
  },
  defaults: {
    avatarUrl: "",
    birthday: "",
    city: "",
    country: "",
    externalId: "",
    firstName: "",
    iconUrl: "",
    id: null,
    isSubscribed: null,
    lastName: "",
    nickName: null,
    photoUrl: "",
    resource: "",
    screenName: ""
  }
});
