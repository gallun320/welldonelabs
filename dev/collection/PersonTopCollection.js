var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var model = require('./../model/PersonTopModel');

module.exports = Backbone.Collection.extend({
  model: model,
  url: function() {
    return '/api/v1/persons/top';
  },
  parse: function(resp) {
    return resp.response.persons;
  }
});
