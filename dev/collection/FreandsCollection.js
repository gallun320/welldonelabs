var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var model = require('./../model/PersonTopModel');

module.exports = Backbone.Collection.extend({
  limit: 15,
  offset: 0,
  us_id: null,
  initialize: function(options) {
    this.us_id = options.us_id;
    this.limit = options.limit;
    if(this.limit === undefined) this.limit = 15;
  },
  model: model,
  url: function() {
    return '/api/v1/persons/' + this.us_id + '/friends?limit=' + this.limit + '&offset=' + this.offset;
  },
  parse: function(resp) {
    return resp.response.friends;
  },
  nextPage: function() {
    ++this.offset;
    this.fetch({
      error: function() {
        Backbone.Events.trigger('noPage');
      }
    });
  },
  prevPage: function() {
    if(this.offset <= 0) return false;
    --this.offset;
    this.fetch();
  }
});
