var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var model = require('./../model/PublishModel');
var Collection = require('./FreandsCollection');

module.exports = Collection.extend({
  initialize: function() {
    //console.log(this.limit, this.offset);
  },
  model: model,
  url: function() {
    return '/api/v1/subscriptions?limit=' + this.limit + '&offset=' + this.offset;
  },
  parse: function(resp) {
    return resp.response.persons;
  }
});
