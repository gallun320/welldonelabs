var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var Collection = require('./AboutTopCollection');

module.exports = Collection.extend({
  limit: 15,
  offset: 0,
  url: function() {
    return '/api/v1/feedbacks?limit=' + this.limit + '&offset=' + this.offset;
  },
  nextPage: function() {
    ++this.offset;
    this.fetch({
      error: function() {
        Backbone.Events.trigger('noPage');
      }
    });
  },
  prevPage: function() {
    if(this.offset <= 0) return false;
    --this.offset;
    this.fetch();
  }
});
