var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var model = require('./../model/RegModel');

module.exports = Backbone.Collection.extend({
  limit: 0,
  initialize: function() {
    //console.log('RegCollection init');
    var width = $(window).width();
    this.limit = Math.floor(width / 144);
  },
  model: model,
  url: function() {
    return '/api/v1/persons/random?limit=' + this.limit;
  },
  parse: function(resp) {
    return resp.response.persons;
  }
});
