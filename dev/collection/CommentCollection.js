var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var model = require('./../model/CommentModel');

module.exports = Backbone.Collection.extend({
  us_id: null,
  initialize: function(options) {
    this.us_id = options.us_id;
  },
  model: model,
  url: function() {
    return '/api/v1/feedbacks/'+ this.us_id +'/comments'
  },
  parse: function(resp) {
    return resp.response.comments;
  }
});
