var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var model = require('./../model/AboutTopModel');

module.exports = Backbone.Collection.extend({
  model: model,
  url: function() {
    return '/api/v1/feedbacks/top';
  },
  parse: function(resp) {
    return resp.response.feedbacks;
  }
});
