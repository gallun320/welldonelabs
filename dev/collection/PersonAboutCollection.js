var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var model = require('./../model/AboutTopModel');
//var AboutTopCollection = require('./AboutTopCollection');

module.exports = Backbone.Collection.extend({
  us_id: null,
  initialize: function(options) {
    this.us_id = options.us_id;
    //console.log('PersonAboutCollection init');
    this.fetch();
  },
  model: model,
  url: function() {
    return '/api/v1/persons/' + this.us_id + '/VK';
  },
  parse: function(resp) {
    return resp.response.feedbacks;
  }
});
