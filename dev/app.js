var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var Marionette = Backbone.Marionette = require('backbone.marionette');
var Router = require('./route/Router');
var Controller = require('./controller/Controller');
var docCookies = require('./lib/cookie');

window.App = new(Marionette.Application.extend({
  initialize: function () {
    console.log('App has init');
  }
}))();


window.App.addRegions(function () {
  return {
    scaffoldingRegion: "#wrap"
  }
});


window.App.addInitializer(function (options) {
  //console.log('Has init');
  if(!window.modelAuth.get('auth') && docCookies.hasItem('inActive') && docCookies.hasItem('lastActive')) {
        window.modelAuth.set('auth', true);
      }
  $(window).off('click').on('click', function() {
    Backbone.Events.trigger('close:click');
  });

   $(window).on('click scroll keyup', function() {
    //console.log('click');
    //if(!window.modelAuth.get('auth')) return false;
    var lastActive = new Date().getTime();
    //console.log(lastActive);
    docCookies.setItem('lastActive', lastActive);
  });
  setInterval(function() {
    if(docCookies.hasItem('inActive') && docCookies.hasItem('lastActive')) {
      var inActive = docCookies.getItem('inActive'),
          lastActive = docCookies.getItem('lastActive'),
          currentTime = new Date().getTime();
      //currentTime = lastActive - currentTime;
      //console.log(inActive, currentTime, lastActive);
      inActive = parseInt(inActive, 10) * 1000;
      lastActive = parseInt(lastActive, 10);
      console.log(currentTime - lastActive, inActive);
      if((currentTime - lastActive) > inActive) {

        window.modelAuth.set('auth', false);
        docCookies.removeItem('lastActive');
        docCookies.removeItem('inActive');
      } else if(!window.modelAuth.get('auth')) {
        window.modelAuth.set('auth', true);
      }
    }
  }, 3000);
  /*window.closePopup = function() {
    $('.popup__wrapper').removeClass('popup-show');
  }*/
  window.baseRoute = new Router({
    controller: new Controller()
  });
  Backbone.history.start({pushState: true});
});

window.App.start();
