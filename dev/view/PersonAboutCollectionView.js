var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var Marionette = Backbone.Marionette = require('backbone.marionette');
var Collection = require('./../collection/PersonAboutCollection');
var Model = require('./../model/PersonModel');
var AboutTopCollectionView = require('./AboutTopCollectionView');
var AboutItemView = require('./AboutItemView');
var moment = require('moment');
var PersonAboutItemView = AboutItemView.extend({
  template: require('./template/person_about--template.html')
});


module.exports = AboutTopCollectionView.extend({
  us_id: null,
  us_col_id: null,
  initialize: function(options) {
    var self = this;
    this.us_id = options.us_id;
    //this.us_col_id = new Model({us_id: this.us_id});
    //this.us_col_id.on('change', this.setCol, this);
    this.collection = new Collection({us_id: this.us_id});
    Backbone.Events.on('feedback:add', function(resp) {
      //console.log(resp.response.feedback);
      resp.response.feedback.createdAt = new Date(resp.response.feedback.createdAt);
      resp.response.feedback.createdAt = moment(resp.response.feedback.createdAt).fromNow();
      self.collection.add(resp.response.feedback);
    });

  },
  onRender: function() {
    var self = this;
    //this.us_id = this.us_col_id.get('feedbacks ');
    /*this.collection = new Collection({us_id: this.us_id});
    this.collection.fetch({ success: function() {
        console.log('PersonAboutCollection', 'fetch');
        //self.render();
      }});*/
  },
  childView: PersonAboutItemView

});
