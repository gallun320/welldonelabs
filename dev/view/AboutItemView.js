var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var Marionette = Backbone.Marionette = require('backbone.marionette');
//var Collection = require('./../collection/AboutTopCollection');
//var AboutTopCollectionView = require('./AboutTopcollectionView');
var CommentCollection = require('./../collection/CommentCollection');

module.exports = Backbone.Marionette.ItemView.extend({
  modelEvents: {
    'change': 'render'
  },
  className: 'user-list__content',
  attributes: {
    small: "9 centerd"
  },
  initialize: function() {
    _.bindAll(this, 'commentAdd', 'comment', 'disProof', 'proof');
    //console.log('AboutLastItemView init');
    this.listenTo(Backbone.Events, 'close:click', this.heightDown);
  },
  template: require('./template/last_about--template.html'),
  ui: {
    form: 'textarea',
    btn_comment: '.about__item__btn_comment',
    btn_send: '.about__input__btn',
    okBtn: '.about__btn_access__ok',
    noBtn: '.about__btn_access__no',
    nickLink: '.nick'
  },
  events: {
    'click @ui.form': 'heightMore',
    'click @ui.btn_comment': 'comment',
    'click @ui.btn_send': 'commentAdd',
    'click @ui.okBtn': 'proof',
    'click @ui.noBtn': 'disProof',
    'click @ui.nickLink': 'nickPage'
  },
  nickPage: function() {
    window.baseRoute.navigate('#person/' + this.model.get('person').screenName, {trigger: true});
  },
  proof: function() {
    var self = this;
    if(!window.modelAuth.get('auth')) {
      Backbone.Events.trigger('popupHeader popupHeader:enter');
      return false;
    }
    var id = this.model.get('id');
    /*if(this.ui.okBtn.hasClass('click') === true) return false;
    this.ui.okBtn.addClass('click');
    //var i = this.model.get('proofCount');
    //this.model.set('proofCount', ++i);
    $.post('/api/v1/confirms', {feedbackId: id, confirmType: 'Proof'}, function(resp) {    var i = self.model.get('proofCount');
      self.model.set('proofCount', ++i);
      self.model.set('rating', resp.response.reputation);
     self.render();
    });*/
    $.ajax({
      url: '/api/v1/confirms',
      type: 'POST',
      data: {feedbackId: id, confirmType: 'Proof'},
      success: function(resp) {
      var i = self.model.get('proofCount');
      self.model.set('proofCount', ++i);
      self.model.set('rating', resp.response.reputation);
      self.render();

      },

      error: function(resp) {
        self.$el.find('.proof--error h2').html(resp.responseJSON.error);
      //$('.popup__wrapper').addClass('popup-show');
      self.$el.find('.proof--error').css('opacity', 1);
      setInterval(function() {
        self.$el.find('.proof--error').css('opacity', 0);
      }, 3000);
      return false;
      }
    });
  },
  disProof: function() {
    var self = this;
    if(!window.modelAuth.get('auth')) {
      Backbone.Events.trigger('popupHeader popupHeader:enter');
      return false;
    }
    var id = this.model.get('id');
    /*if(this.ui.noBtn.hasClass('click') === true) return false;
    this.ui.noBtn.addClass('click');
    //var i = this.model.get('disproofCount');
    //this.model.set('disproofCount', ++i);
    $.post('/api/v1/confirms', {feedbackId: id, confirmType: 'Disproof'}, function(resp) {

    });*/
    $.ajax({
      url: '/api/v1/confirms',
      type: 'POST',
      data: {feedbackId: id, confirmType: 'Disproof'},
      success: function(resp) {

        var i = self.model.get('disproofCount');
      self.model.set('disproofCount', ++i);
      Backbone.Events.trigger('reputation:send', resp.response.reputation);
     self.render();

      },

      error: function(resp) {
        self.$el.find('.disProof--error h2').html(resp.responseJSON.error);
      //$('.popup__wrapper').addClass('popup-show');
      self.$el.find('.disProof--error').css('opacity', 1);
      setInterval(function() {
        self.$el.find('.disProof--error').css('opacity', 0);
      }, 3000);
      return false;
      }
    });
  },
  heightMore: function() {
    this.$el.find('textarea').css({height: 250});
    return false;
  },
  comment: function() {
    console.log('Comment');
    var self = this;
    var collection = new CommentCollection({us_id: this.model.get('id')});
    if(this.ui.btn_comment.hasClass('click')) {
      this.model.attributes.data = [];
      this.render();
    } else {

      collection.fetch({success: function() {
        self.model.attributes.data = collection.models;
        self.render();
        self.ui.btn_comment.addClass('click');
      }});
    }

  },
  commentAdd: function() {
    var self = this;
    if(!window.modelAuth.get('auth')) {
      Backbone.Events.trigger('popupHeader popupHeader:enter');
      return false;
    }
    var data = this.$el.find('textarea').val();
    var id = this.model.get('id');
    /*$.post('/api/v1/comments', {feedbackId: id, text: data},
           function() {
      var count = self.model.get('commentCount');
      self.model.set('commentCount', ++count);
      self.model.attributes.data.push({attributes: {text: data}});
      self.model.trigger('change');
      self.render();
    });*/
    if(data === '') {
      this.$el.find('.comment_error h2').html('Добавьте текст');
      //$('.popup__wrapper').addClass('popup-show');
      this.$el.find('.comment_error').css('opacity', 1);
      setInterval(function() {
        self.$el.find('.comment_error').css('opacity', 0);
      }, 4000);
      return false;
    }
    $.ajax({
      url: '/api/v1/comments',
      type: 'POST',
      data: {feedbackId: id, text: data},
      success: function(resp) {

        var count = self.model.get('commentCount');
      self.model.set('commentCount', ++count);
      self.model.attributes.data.push({attributes: {text: data}});
      self.model.trigger('change');
      self.render();
      },

      error: function(resp) {
        self.$el.find('.comment_error h2').html(resp.responseJSON.error);
      //$('.popup__wrapper').addClass('popup-show');
      self.$el.find('.comment_error').css('opacity', 1);
      setInterval(function() {
        self.$el.find('.comment_error').css('opacity', 0);
      }, 3000);
      return false;
      }
    });
  },
  heightDown: function() {
    //console.log('Event');
    this.$el.find('textarea').css({height: 55});
  }
});
