var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var Marionette = Backbone.Marionette = require('backbone.marionette');
var PersonModel = require('./../model/PersonModel');
var Collection = require('./../collection/FreandsCollection');

//var FreandsCollectionView = require('./FreandsCollectionView');
module.exports = Backbone.Marionette.ItemView.extend({
  modelEvents: {
    'change': 'render'
  },
  us_id: null,
  className: 'user-list__content nav-inline',
  attributes: {

    'small': '9 centerd'
  },
  initialize: function(options) {
    _.bindAll(this, 'tmpl');
    this.us_id = options.us_id;
    //console.log('PersonIremView init', this.us_id);
    this.model = new PersonModel({us_id: this.us_id});
    this.model.set('data', []);
    this.listenTo(this.model, 'change', this.tmpl);
    //this.tmpl();
    //console.log(window.idUser);
    Backbone.Events.on('reputation:send', function(resp) {
      this.model.attributes.stats.reputation = resp;
      this.model.trigger('change');
    }, this);
  },
  ui: {
    link: '.person__gallery__text span',
    friendItem: '.person__gallery__item',
    subsAdd: '.btn__add',
    subsDelete: '.user-list__item__btn-delte'
  },
  events: {
    'click @ui.friendItem': 'friendPage',
    'click @ui.link': 'route',
    'click @ui.subsAdd': 'subsAddSend',
    'click @ui.subsDelete': 'subsDeleteSend'
  },
  friendPage: function(e) {
    var data = this.$el.find(e.currentTarget).attr('data-page');
   // console.log(data);
  },
  tmpl: function() {
    var countLimit = this.$el.find('.person__gallery__wrapper').width();
    countLimit = Math.floor(countLimit / 66);
    var self = this;
    var collection = new Collection({us_id: self.model.get('id'), limit: countLimit});
    collection.fetch({
      success: function() {
        self.model.attributes.data = collection.models;
        self.render();
        //self.stopListening('change');
      }
    });
  },
  template: require('./template/person--template.html'),
  onRender: function() {
    var self = this;
    var stats = this.model.get('stats');
    if(stats !== undefined && stats.reputation !== 0 ) {
      this.$el.find('.reputation__width').css({'width': stats.reputation + '%'});
      this.$el.find('.user-list__item__count').html(stats.reputation + '%');
      var minus = 100 - stats.reputation ;
      this.$el.find('.user-list__item__count-minus').html(minus + '%');
    }
    this.$el.find('.person__gallery__item').click(function(e) {
      e.stopImmediatePropagation();
      if(e.which === 2) return false;
      var data = self.$el.find(e.currentTarget).attr('data-page');
      window.baseRoute.navigate('#person/' + data, {trigger: true});
    });
  },
  route: function() {
    window.baseRoute.navigate('#person/' + this.model.get('id') + '/friends', {trigger: true});
  },
  subsAddSend: function() {
    var self = this;
    if(!window.modelAuth.get('auth')) {
      Backbone.Events.trigger('popupHeader popupHeader:enter');
      return false;
    }
    $.post('/api/v1/subscriptions', {personId: this.model.get('id')}, function() {
      self.model.set('isSubscribed', true);
      self.render();
    });
  },
  subsDeleteSend: function() {
    var self = this;
    if(!window.modelAuth.get('auth')) {
      Backbone.Events.trigger('popupHeader popupHeader:enter');
      return false;
    }
    $.ajax({
      url: '/api/v1/subscriptions?personId=' + this.model.get('id'),
      type: 'DELETE',
      success: function() {
        self.model.set('isSubscribed', false);
        self.render();
      }
    });
  }
});
