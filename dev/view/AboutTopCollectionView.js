var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var Marionette = Backbone.Marionette = require('backbone.marionette');
var Collection = require('./../collection/AboutTopCollection');
var AboutItemView = require('./AboutItemView');

var AboutTopItemView = AboutItemView.extend({
  initialize: function() {
    this.listenTo(Backbone.Events, 'place:change', this.setPlace);
    this.listenTo(Backbone.Events, 'close:click', this.heightDown);
    this.listenTo(this, 'render', this.setPlace);
  },
  template: require('./template/top_about--template.html'),
  setPlace: function() {
    var self = this;
    if(this.model.get('place') !== "") {
      //this.$el.find('.user-list__item__place').html('#' + self.model.get('place'));
      switch(this.model.get('place')) {
        case "1":
          this.$el.find('.place__icon').addClass('place__icon--first');
          break;
        case "2":
          this.$el.find('.place__icon').addClass('place__icon--second');
          break;
        case "3":
          this.$el.find('.place__icon').addClass('place__icon--third');
          break;
        default:
          break;
      }
    }
  }
});


module.exports = Backbone.Marionette.CollectionView.extend({
  className: 'row-full',
  childView: AboutTopItemView,
  collection: new Collection(),
  onRender: function() {
    var self = this;
    this.collection.fetch({
      success: function() {
        if(self.collection.models.length <= 1) {
          self.collection.models[0].attributes.place = "1";
        } else if(self.collection.models.length > 1 && self.collection.models.length < 3) {
          self.collection.models[0].attributes.place = "1";
          self.collection.models[1].attributes.place = "2";
        } else {
          self.collection.models[0].attributes.place = "1";
          self.collection.models[1].attributes.place = "2";
          self.collection.models[2].attributes.place = "3";
        }
        Backbone.Events.trigger('place:change');

        console.log(self.collection.models);
      }
    });
  }
});
