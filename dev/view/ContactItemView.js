var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var Marionette = Backbone.Marionette = require('backbone.marionette');

module.exports = Backbone.Marionette.ItemView.extend({
  initialize: function() {
    var self = this;
    this.$el.find('.popup__wrapper').click(self.closePopup);
    console.log('init');
  },
  ui: {
    closeBtn: '.icon__cross',
    btnSend: '.contact__btn',
    closePopupBtn: '.popup__wrapper'
  },
  events: {
    'click @ui.closeBtn': 'close',
    'click @ui.btnSend': 'send',
    'click @ui.closePopupBtn': 'closePopup'
  },
  close: function(){
    //console.log('click');
    window.history.back();
  },
  send: function() {
    var self = this;
    var name = this.$el.find('.name').val();
    var email = this.$el.find('.e_mail').val();
    var theme = this.$el.find('.theme').val();
    var msg = this.$el.find('.msg').val();
    if(name.length > 3 && email.length > 6 && theme.length > 3 && msg.length > 5) {
      $.post('https://nomask.fm/api/v1/contacts', {clientName: name, clientEmail: email, subject: theme, message: msg}, function(resp) {
        self.$el.find('.popup__wrapper').addClass('popup-show');
        self.$el.find('.name').val('');
        self.$el.find('.e_mail').val('');
        self.$el.find('.theme').val('');
        self.$el.find('.msg').val('');
      });
    }
  },
  closePopup: function() {
    this.$el.find('.popup__wrapper').removeClass('popup-show');
  },
  template: require('./template/contact--template.html')
});
