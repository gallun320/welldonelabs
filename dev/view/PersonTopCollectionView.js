var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var Marionette = Backbone.Marionette = require('backbone.marionette');
var Collection = require('./../collection/PersonTopCollection');

var PersonTopItemView = Backbone.Marionette.ItemView.extend({
  modelEvents: {
    'change': 'render'
  },
  className: 'user-list__content',
  attributes: {
    small: "9 centerd"
  },
  ui: {
    nickLink: ".user-list__item__nick",
    photoLink: ".user-list__item__photo"
  },
  events: {
    "click @ui.nickLink": "nickPage",
    "click @ui.photoLink": "nickPage"
  },
  nickPage: function() {
    window.baseRoute.navigate('#person/' + this.model.get('screenName'), {trigger: true});
  },
  initialize: function() {
    this.listenTo(Backbone.Events, 'place:change', this.setPlace);
  },
  onRender: function() {
    var stats = this.model.get('stats');
    if(stats !== undefined && stats.reputation !== 0 ) {
      this.$el.find('.reputation__width').css({'width': stats.reputation + '%'});
      this.$el.find('.user-list__item__count').html(stats.reputation + '%');
      var minus = 100 - stats.reputation ;
      this.$el.find('.user-list__item__count-minus').html(minus + '%');
    }
  },
   template: require('./template/person_top--template.html'),
  setPlace: function() {
    var self = this;
    if(this.model.get('place') !== "") {
      this.$el.find('.user-list__item__place').html('#' + self.model.get('place'));
      switch(this.model.get('place')) {
        case "1":
          this.$el.find('.user-list__item__photo').addClass('user-list__item-first');
          break;
        case "2":
          this.$el.find('.user-list__item__photo').addClass('user-list__item-second');
          break;
        case "3":
          this.$el.find('.user-list__item__photo').addClass('user-list__item-third');
          break;
        default:
          break;
      }
    }
  }
});

module.exports = Backbone.Marionette.CollectionView.extend({
  className: 'row-full',
  childView: PersonTopItemView,
  collection: new Collection(),
  onRender: function() {
    var self = this;
    this.collection.fetch({
      success: function() {
        if(self.collection.models.length <= 1) {
          self.collection.models[0].attributes.place = "1";
        } else if(self.collection.models.length > 1 && self.collection.models.length < 3) {
          self.collection.models[0].attributes.place = "1";
          self.collection.models[1].attributes.place = "2";
        } else {
          self.collection.models[0].attributes.place = "1";
          self.collection.models[1].attributes.place = "2";
          self.collection.models[2].attributes.place = "3";
        }
        Backbone.Events.trigger('place:change');

      }
    });
  },
  onBeforeShow: function() {

  }
});
