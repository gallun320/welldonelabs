var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var Marionette = Backbone.Marionette = require('backbone.marionette');
var Collection = require('./../collection/FreandsCollection');

var PersonFreandItemView = Backbone.Marionette.ItemView.extend({
  modelEvents: {
    'change': 'render'
  },
  className: 'user-list__content',
  initialize: function() {
  },

  onRender: function() {
    var stats = this.model.get('stats');
    if(stats !== undefined && stats.reputation !== 0 ) {
      this.$el.find('.reputation__width').css({'width': stats.reputation + '%'});
      this.$el.find('.user-list__item__count').html(stats.reputation + '%');
      var minus = 100 - stats.reputation ;
      this.$el.find('.user-list__item__count-minus').html(minus + '%');
    }
  },
   template: require('./template/person_freand--template.html'),
  ui: {
    nickLink: ".user-list__item__nick",
    photoLink: ".user-list__item__photo"
  },
  events: {
    "click @ui.nickLink": "nickPage",
    "click @ui.photoLink": "nickPage"
  },
  nickPage: function() {
    window.baseRoute.navigate('#person/' + this.model.get('screenName'), {trigger: true});
  }
});

module.exports = Backbone.Marionette.CompositeView.extend({
  us_id: null,
  initialize: function(options) {
    this.us_id = options.us_id;
    this.collection = new Collection({us_id: this.us_id});
    this.collection.fetch();
    this.listenToOnce(Backbone.Events, 'noPage', this.errPage);

  },
  ui: {
    next: '.arrow_right',
    prev: '.arrow_left'
  },
  events: {
    'click @ui.next': 'nextPage',
    'click @ui.prev': 'prevPage'
  },
  nextPage: function() {
    this.collection.nextPage();
    this.render();
  },
  prevPage: function() {
    this.collection.prevPage();
    this.render();
  },
  errPage: function() {
    var self = this;
    //this.$el.find('.err_page').css('display', 'block');
    this.$el.find('.err_page').css('opacity', 1);
    setTimeout(function() {
      self.$el.find('.err_page').css('opacity', 0);
    }, 2000);
  },
  className: 'row-full',
  childView: PersonFreandItemView,
  onRender: function() {

  },
  template: require('./template/about_last_composite--template.html'),
  childViewContainer: '.user-list__content'
});
