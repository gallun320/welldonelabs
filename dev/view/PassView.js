var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var Marionette = Backbone.Marionette = require('backbone.marionette');

module.exports = Backbone.Marionette.ItemView.extend({
  token: null,
  className: 'user-list__content',
  attributes: {
    small: "9 centerd"
  },
  ui: {
    btn: '.btn',
    closePopupBtn: '.popup__wrapper'
  },
  events: {
    'click @ui.btn': 'resPass',
    'click @ui.closePopupBtn': 'closePopup'
  },
  initialize: function(opthions) {
    this.token = opthions.token;

  },
  resPass: function() {
    var self = this;
    var data = this.$el.find('input').val();
    $.ajax({
      url: '/api/v1/accounts/reset_password/' + self.token + '/?password=' + data,
      type: 'PUT',
      success: function(resp) {
        self.$el.find('.popup__wrapper').addClass('popup-show');
        self.$el.find('input').val('');
      }
    });
  },
  closePopup: function() {
    this.$el.find('.popup__wrapper').removeClass('popup-show');
  },
  template: require('./template/resset_passowrd--template.html')
});
