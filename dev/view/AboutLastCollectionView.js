var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var Marionette = Backbone.Marionette = require('backbone.marionette');
var Collection = require('./../collection/AboutCollection');
var AboutTopCollectionView = require('./AboutTopcollectionView');
var CommentCollection = require('./../collection/CommentCollection');
var AboutItemView = require('./AboutItemView');

var AboutLastItemView = AboutItemView.extend({
  attributes: {
    small: "12"
  }
});

module.exports = Backbone.Marionette.CompositeView.extend({
  className: 'row-full',
  initialize: function() {
    //console.log('LastCollectionView init', this.children);
    this.listenToOnce(Backbone.Events, 'noPage', this.errPage);
  },
  collection: new Collection(),
  onRender: function() {
    this.collection.fetch();
  },
  ui: {
    next: '.arrow_right',
    prev: '.arrow_left'
  },
  events: {
    'click @ui.next': 'nextPage',
    'click @ui.prev': 'prevPage'
  },
  nextPage: function() {
    this.collection.nextPage();
    this.render();
    $(window).scrollTop(0);
  },
  prevPage: function() {
    this.collection.prevPage();
    this.render();
    $(window).scrollTop(0);
  },
  errPage: function() {
    var self = this;
    //this.$el.find('.err_page').css('display', 'block');
    this.$el.find('.err_page').css('opacity', 1);
    setTimeout(function() {
      self.$el.find('.err_page').css('opacity', 0);
    }, 2000);
  },
  childView: AboutLastItemView,
  template: require('./template/about_last_composite--template.html'),
  childViewContainer: '.user-list__content'
});
