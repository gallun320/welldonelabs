var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var Marionette = Backbone.Marionette = require('backbone.marionette');
var HeaderView = require('./HeaderItemView');
window.headerView = new HeaderView();
module.exports = new (Backbone.Marionette.LayoutView.extend({
  className: 'layout',
  initialize: function() {
    //console.log('Layout init');
    //this.showChildView('appHeader', headerView);
  },
  template: require('./template/person_conatainer--template.html'),
  regions: {
    appModal: "#modal",
    profileContent: "#profile_content",
    barContent: "#bar_content",
    commentContent: "#abot_content",
    appHeader: "#header"
  },
  onRender: function() {
    if(window.headerView.isDestroyed) {
      window.headerView = new HeaderView();
      this.showChildView('appHeader', window.headerView);
    } else {
      window.headerView.render();
    }
    this.showChildView('appHeader', window.headerView);

  }
}))();
