var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var Marionette = Backbone.Marionette = require('backbone.marionette');

module.exports = Backbone.Marionette.ItemView.extend({
  token: null,
  className: 'user-list__content',
  attributes: {
    small: "9 centerd"
  },
  initialize: function(opthions) {
    this.token = opthions.token;
    var self = this;
    //this.$el.find('h1').html('E-mail уже подтвержден');
    $.ajax({
      url: '/api/v1/accounts/confirm_email?regToken=' + this.token,
      type: 'PUT',
      success: function(resp) {

        self.$el.find('h1').html("Email подтвержден");

      },

      error: function(resp) {
        self.$el.find('h1').html("Email уже подтвержден");
      }
    });
  },
  template: require('./template/confirm--template.html')
});
