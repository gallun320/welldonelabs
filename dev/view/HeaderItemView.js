var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var Marionette = Backbone.Marionette = require('backbone.marionette');
var docCookies = require('./../lib/cookie');
var model = require('./../model/ModalAuthModel');
window.modelAuth = new model();


module.exports = Backbone.Marionette.ItemView.extend({
  modelEvents: {
    'change': 'render'
  },
  className: 'header__search nav-inline',
  attributes: {
    'space': 'a'
  },
  initialize: function () {

    console.log('HeaderItemView');
    //docCookies.removeItem('SESSION');
  },
  model: window.modelAuth,
  template: require('./template/header--template.html'),
  ui: {
    popupBtn: '.icon__circle-menu',
    searchLink: '.search_link',
    aboutTopLink: '.about_top_link',
    personTopLink: '.person_top_link',
    aboutLastLink: '.about_last_link'
  },
  events: {
    'click @ui.popupBtn': 'popupShow',
    'click @ui.searchLink': 'searchPage',
    'click @ui.aboutTopLink': 'aboutTopPage',
    'click @ui.personTopLink': 'personTopPage',
    'click @ui.aboutLastLink': 'aboutLastPage'
  },
  searchPage: function() {
    window.baseRoute.navigate('#',{trigger: true});
  },
  aboutTopPage: function() {
    window.baseRoute.navigate('#abouttop',{trigger: true});
  },
  personTopPage: function() {
    window.baseRoute.navigate('#ptop',{trigger: true});
  },
  aboutLastPage: function() {
    window.baseRoute.navigate('#aboutlast',{trigger: true});
  },
  popupShow: function () {
    this.$el.find('.list-dropdown').addClass('popup-show');
    return false;
  },
  onRender: function() {
    var self = this;
    _.bindAll(this, 'exit');
    //var cookie = docCookies.getItem('SESSION');
    //if(cookie !== null) this.model.set('auth', true);
    //console.log(cookie);
    /*setTimeout(function() {
      console.log('click');

      self.$el.find('.enter').click(self.enter);
    }, 10000)*/
    //var self = this;
    this.listenTo(Backbone.Events, 'close:click', this.popupClose);
     this.$el.find('.reg').click(self.reg);
    this.$el.find('.enter').click(self.enter);
    this.$el.find('.list_icon__pod').click(self.pod);
    this.$el.find('.list_icon__exit').off('click').on('click', self.exit);
  },
  popupClose: function() {
     this.$el.find('.list-dropdown').removeClass('popup-show');
  },
  onBeforeShow: function () {
   /* var self = this;
    window.regAuth = function() {
      self.reg();
    }
    //console.log(this.$el.find('.reg').click());
    this.$el.find('.reg').click(self.reg);
    this.$el.find('.enter').click(self.enter);
    this.$el.find('.list_icon__pod').click(self.pod);
    this.$el.find('.list_icon__exit').click(self.exit);*/
  },
  reg: function() {
    Backbone.Events.trigger('popupHeader popupHeader:reg');
    //Backbone.Events.trigger('popupHeader');
  },
  enter: function() {
    Backbone.Events.trigger('popupHeader popupHeader:enter');
    //Backbone.Events.trigger('popupHeader');
  },
  pod: function() {
    window.baseRoute.navigate('#publish', {trigger: true});
  },
  exit: function() {
    var self = this;
    $.get('/logout', function(resp) {
      window.modelAuth.set('auth', false);
      docCookies.removeItem('lastActive');
      docCookies.removeItem('inActive');
      self.render();
    });
  }
});
