var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var Marionette = Backbone.Marionette = require('backbone.marionette');
var HeaderView = require('./HeaderItemView');
window.headerView = new HeaderView();
module.exports = new (Backbone.Marionette.LayoutView.extend({
  className: 'layout',
  initialize: function() {
    //console.log('Layout init');
    //this.showChildView('appHeader', headerView);
  },
  template: require('./template/scaffolding--template.html'),
  regions: {
    appModal: "#modal",
    appRegion: "#content",
    appHeader: "#header"
  },
  onRender: function() {
    var self = this;
    if(window.headerView.isDestroyed) {
      window.headerView = new HeaderView();
      this.showChildView('appHeader', window.headerView);
    } else {
      window.headerView.render();
    }
    this.showChildView('appHeader', window.headerView);
    this.$el.find('.contact_link').click(self.contactLink);
    this.$el.find('.project_link').click(self.projectLink);
    this.$el.find('.rule_link').click(self.ruleLink);
  },
  contactLink: function() {
    window.baseRoute.navigate('#contacts', {trigger: true});
  },
  projectLink: function() {
    window.baseRoute.navigate('#about', {trigger: true});
  },
  ruleLink: function() {
    window.baseRoute.navigate('#rule', {trigger: true});
  }
}))();
