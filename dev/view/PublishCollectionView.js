var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var Marionette = Backbone.Marionette = require('backbone.marionette');
var Collection = require('./../collection/PublishCollection');

var ItemView = Backbone.Marionette.ItemView.extend({
  className: 'user-list__content',
  attributes: {

    "small": "12"
  },
  modelEvents: {
    'change': 'render'
  },

  onRender: function() {
    /*var stats = this.model.get('stats');
    if(stats !== undefined && stats.reputation !== 0 ) {
      this.$el.find('.reputation__width').css({'width': stats.reputation + '%'});
      this.$el.find('.user-list__item__count').html(stats.reputation + '%');
      var minus = 100 - stats.reputation ;
      this.$el.find('.user-list__item__count-minus').html(minus + '%');
    }*/
  },
  ui: {
    nickLink: '.user-list__item__nick',
    photoLink: ".user-list__item__photo",
    subsAdd: '.btn__add',
    subsDelete: '.user-list__item__btn-delte'
  },
  events: {
    'click @ui.nickLink': 'nickPage',
    'click @ui.subsAdd': 'subsAddSend',
    'click @ui.subsDelete': 'subsDeleteSend',
    "click @ui.photoLink": "nickPage"
  },
  nickPage: function() {
    window.baseRoute.navigate('#person/' + this.model.get('screenName'), {trigger: true});
  },
  subsDeleteSend: function() {
    var self = this;
    $.ajax({
      url: '/api/v1/subscriptions?personId=' + this.model.get('id'),
      type: 'DELETE',
      success: function() {
        self.model.set('isSubscribed', false);
        self.render();
      }
    });
  },
   subsAddSend: function() {
    var self = this;
    $.post('/api/v1/subscriptions', {personId: this.model.get('id')}, function() {
      self.model.set('isSubscribed', true);
      self.render();
    });

  },
  template: require('./template/publish--template.html')
});


module.exports = Backbone.Marionette.CompositeView.extend({
  attributes: {
    small: "9 centerd"
  },
  initialize: function() {
    var self = this;
    /*this.collection.fetch({
      error: function() {
        $('.err_auth').css('display','block');
      }
    });*/
    if(window.modelAuth.get('auth')) {
      //this.collection = new Collection();
      this.collection.fetch({
        success: function() {
          self.render();
        }
      });

    }

  },
  onRender: function() {

  },
  childView: ItemView,
  collection:  new Collection(),
  childViewContainer: '.user-list__content',
  template: require('./template/publish_container--template.html')
});
