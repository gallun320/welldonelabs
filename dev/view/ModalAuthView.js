var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var Marionette = Backbone.Marionette = require('backbone.marionette');
var docCookies = require('./../lib/cookie');

module.exports = Backbone.Marionette.ItemView.extend({
  initialize: function () {

  },
  enter: function () {
    this.$el.find('.enter').addClass('selected');
    this.$el.find('#cd-login').addClass('is-selected');
  },
  reg: function () {
    this.$el.find('.reg').addClass('selected');
    this.$el.find('#cd-signup').addClass('is-selected');
  },
  className: ' ',
  template: require('./template/modal_auth--template.html'),
  onRender: function () {
    this.listenTo(Backbone.Events, 'popupHeader:enter', this.enter);
    this.listenTo(Backbone.Events, 'popupHeader:reg', this.reg);
    //this.fn();
  },
  ui: {
    form_modal: '.cd-user-modal',
    form_login: '#cd-login',
    form_signup: '#cd-signup',
    form_forgot_password: '#cd-reset-password',
    form_modal_tab: '.cd-switcher',
    tab_login: '.cd-switcher li:first a',
    tab_signup: '.cd-switcher li:last a',
    forgot_password_link: '.cd-forgot-password-link',
    back_to_login_link: '.cd-back-to-login-link',
    btn_log: '.login',
    btn_create: '.create_ac',
    btn_reset: '.reset_em'
  },
  events: {
    'click @ui.btn_log': 'enterLog',
    'click @ui.btn_create': 'createAc',
    'click @ui.btn_reset': 'resetEm'
  },
  enterLog: function() {
    var self = this;
    var email = this.$el.find('#signin-email').val();
    var password = this.$el.find('#signin-password').val();
    var passReg = /^(?=.*[0-9])(?=.*[a-z]).{6,}/gi;
    var emailReg = /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/gi;
    if( password === '' && email === '') {
      this.$el.find('.cd-error-message-in').addClass('is-visible');
      return false;
    } else if(password === '') {
      this.$el.find('.cd-password-error-message').html('Заполните поле');
      this.$el.find('.cd-password-error-message').addClass('is-visible');
      return false;
    } else if(email === '') {
      this.$el.find('.cd-email-error-message').html('Заполните поле');
      this.$el.find('.cd-email-error-message').addClass('is-visible');
      return false;
    }
    if(!passReg.test(password)) {
      this.$el.find('.cd-password-error-message').html('Пароль не корректен');
      this.$el.find('.cd-password-error-message').addClass('is-visible');
      return false;
    }

    if(!emailReg.test(email)) {
      this.$el.find('.cd-email-error-message').html('Email не корректен');
      this.$el.find('.cd-email-error-message').addClass('is-visible');
      return false;
    }
    /*$.post('/login', {login: email, password: password},function() {
       self.ui.form_modal.removeClass('is-visible');
      window.modelAuth.set('auth', true);
    });*/
     $.ajax({
      url: '/login',
      type: 'POST',
      data: {login: email, password: password},
      success: function(resp) {
        console.log(resp)
        //self.model.set('isSubscribed', false);
        //self.ui.form_modal.removeClass('is-visible');
        self.ui.form_modal.removeClass('is-visible');
        window.modelAuth.set('auth', true);

        var inActive = resp.inactiveInterval;
        docCookies.setItem('inActive', inActive);
        //docCookies.setItem('lastActive', inActive);
      },
      error: function(resp) {
        var data = resp.responseJSON;
        self.$el.find('.cd-password-error-message').html(data.error);
        self.$el.find('.cd-password-error-message').addClass('is-visible');
      }
    });
  },
  createAc: function() {
    var self = this;
    //var name = this.$el.find('#signup-username').val();
    var email = this.$el.find('#signup-email').val();
    var password = this.$el.find('#signup-password').val();
    var passReg = /^(?=.*[0-9])(?=.*[a-z]).{6,}/gi;
    var emailReg = /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/gi;
    if( password === '' && email === '') {
      this.$el.find('.cd-error-message-up').addClass('is-visible');
      return false;
    } else if(password === '') {
      this.$el.find('.cd-password-error-message-up').html('Заполните поле!');
      this.$el.find('.cd-password-error-message-up').addClass('is-visible');
      return false;
    } else if(email === '') {
      this.$el.find('.cd-email-error-message-up').html('Заполните поле!');
      this.$el.find('.cd-email-error-message-up').addClass('is-visible');
      return false;
    }
    if(!passReg.test(password)) {
      this.$el.find('.cd-password-error-message-up').html('Некорректный пароль!');
      this.$el.find('.cd-password-error-message-up').addClass('is-visible');
      return false;
    }

    if(!emailReg.test(email)) {
      this.$el.find('.cd-email-error-message-up').html('Некорректный e-mail!');
      this.$el.find('.cd-email-error-message-up').addClass('is-visible');
      return false;
    }
    /*$.post('/api/v1/accounts', {email: email, password: password}, function() {
      self.ui.form_modal.removeClass('is-visible');
    } );*/
    $.ajax({
      url: '/api/v1/accounts',
      type: 'POST',
      data: {email: email, password: password},
      success: function() {
        //self.model.set('isSubscribed', false);
        //self.ui.form_modal.removeClass('is-visible');
        //$('.popup__wrapper .popup h2').html('Регистрация выполнена, на Вашу почту выслано письмо с инструкциями подтверждения');
        self.$el.find('.reg_success').css('opacity', 1);
        self.$el.find('.reg_success').css('height', 'auto');
      },
      error: function(resp) {
        var data = resp.responseJSON;
        //data = JSON.parse(data);
        self.$el.find('.cd-email-error-message-up').html(data.error);
        self.$el.find('.cd-email-error-message-up').addClass('is-visible');
      }
    });
  },
  resetEm: function() {
    var self = this;
    var email = this.$el.find('#reset-email').val();
    if( email === '') {
      this.$el.find('.cd-pas-error-message').addClass('is-visible');
      return false;
    }
     $.ajax({
      url: '/api/v1/accounts/reset_password?email=' + email,
      type: 'PUT',
      success: function(resp) {
        self.ui.form_modal.removeClass('is-visible');
      }
    });
    return false;
  },
  onBeforeShow: function () {
    var self = this;
   /* var $form_modal = this.$el.find('.cd-user-modal'),
      $form_login = this.$el.find('#cd-login'),
      $form_signup = this.$el.find('#cd-signup'),
      $form_forgot_password = this.$el.find('#cd-reset-password'),
      $form_modal_tab = this.$el.find('.cd-switcher'),
      $tab_login = $form_modal_tab.children('li').eq(0).children('a'),
      $tab_signup = $form_modal_tab.children('li').eq(1).children('a'),
      $forgot_password_link = $form_login.find('.cd-form-bottom-message a'),
      $back_to_login_link = $form_forgot_password.find('.cd-form-bottom-message a');
*/
    this.$el.find('.cd-user-modal').on('click', function (event) {
      if ($(event.target).is(self.ui.form_modal) || $(event.target).is('.cd-close-form')) {
        self.ui.form_modal.removeClass('is-visible');
      }
    });

    //switch from a tab to another
    this.ui.form_modal_tab.on('click', function (event) {
      event.preventDefault();
      ($(event.target).is(self.ui.tab_login)) ? self.login_selected(): self.signup_selected();
    });


    //show forgot-password form
    this.ui.forgot_password_link.on('click', function (event) {
      event.preventDefault();
      self.forgot_password_selected();
      return false;
    });

    //back to login from the forgot-password form
    this.ui.back_to_login_link.on('click', function (event) {
      event.preventDefault();
      self.login_selected();
      return false;
    });




  },
  login_selected: function() {
      this.ui.form_login.addClass('is-selected');
      this.ui.form_signup.removeClass('is-selected');
      this.ui.form_forgot_password.removeClass('is-selected');
      this.ui.tab_login.addClass('selected');
      this.ui.tab_signup.removeClass('selected');
      //return false;
    },
  signup_selected: function() {
      this.ui.form_login.removeClass('is-selected');
      this.ui.form_signup.addClass('is-selected');
      this.ui.form_forgot_password.removeClass('is-selected');
      this.ui.tab_login.removeClass('selected');
      this.ui.tab_signup.addClass('selected');
    },
  forgot_password_selected: function() {
      this.ui.form_login.removeClass('is-selected');
      this.ui.form_signup.removeClass('is-selected');
      this.ui.form_forgot_password.addClass('is-selected');
    }
});
