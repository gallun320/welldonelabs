var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var Marionette = Backbone.Marionette = require('backbone.marionette');
var PersonModel = require('./../model/PersonModel');
require('jquery-ui/draggable');

module.exports = Backbone.Marionette.ItemView.extend({
  us_id: null,
  className: 'person__bar nav-inline middle-v indent-top',
  initialize: function(options) {
    this.us_id = options.us_id;
    this.listenTo(Backbone.Events, 'close:click', this.heightDown);
    this.model = new PersonModel({us_id: options.us_id});
  },
  attributes: {
    small:"9 centerd",
    space: "a"
  },
  ui: {
    form: 'textarea',
    btnNext: '.is_bar_visible',
    btnPreview: '.is_bar_hidden',
    btnSendFeed: '.about__input__btn',
    btnAccessMsg: '.btn__access'
  },
  events: {
    'click @ui.form': 'heightMore',
    'click @ui.btnNext': 'next',
    'click @ui.btnPreview': 'preview',
    'click @ui.btnSendFeed': 'sendFeed',
    'click @ui.btnAccessMsg': 'accessMsg'
  },
  sendFeed: function() {
    if(!window.modelAuth.get('auth')) {
      Backbone.Events.trigger('popupHeader popupHeader:enter');
      return false;
    }
    var self = this;
    var tx = this.$el.find('.feedback-user').val();
    var rep = this.$el.find('.person__progress__count__img h2').html();
    rep = parseInt(rep, 10);
    var id = this.model.get('id');
    id = parseInt(id, 10);
    //var id = this.us_id;
    var withAlert = false;
    if(this.ui.btnAccessMsg.hasClass('btn__access_active')) withAlert = true;
    /*$.post('/api/v1/feedbacks', {personId: id, text: tx, rating: rep}, function() {
      Backbone.Events.trigger('feedback:add');
    });*/
    $.ajax({
      url: '/api/v1/feedbacks',
      type: 'POST',
      data: {personId: id, text: tx, rating: rep, withAlert: withAlert},
      success: function(resp) {
        Backbone.Events.trigger('feedback:add', resp);
      },
      error: function(resp) {
        console.log(id, tx, rep, withAlert);
        self.$el.find('.bar__popup__content h2').html(resp.responseJSON.error);
      //$('.popup__wrapper').addClass('popup-show');
      self.$el.find('.bar__popup__content').css('opacity', 1);
      setInterval(function() {
        self.$el.find('.bar__popup__content').css('opacity', 0);
      }, 3000);
      return false;
      }
    });
  },
  accessMsg: function() {
    this.ui.btnAccessMsg.addClass('btn__access_active');
  },
  next: function() {
    var self = this;
    var tx = this.$el.find('.feedback-user').val();
    if(!window.modelAuth.get('auth')) {
      Backbone.Events.trigger('popupHeader popupHeader:enter');
      return false;
    }

    if(tx === '') {
      this.$el.find('.bar__popup__content h2').html('Добавьте текст');
      //$('.popup__wrapper').addClass('popup-show');
      this.$el.find('.bar__popup__content').css('opacity', 1);
      setInterval(function() {
        self.$el.find('.bar__popup__content').css('opacity', 0);
      }, 4000);
      return false;
    }
    this.$el.find('.bar_user_text').removeClass('flex-visible');
    this.$el.find('.bar_user_count').addClass('flex-visible');
  },
  preview: function() {
    this.$el.find('.bar_user_text').addClass('flex-visible');
    this.$el.find('.bar_user_count').removeClass('flex-visible');
  },
  template: require('./template/person_bar--template.html'),
  onRender: function() {
    //console.log('PersonBarView');

    //var posOld = 0;
    var self = this;
    var z = 1;
    this.$el.find('.person__progress__count__img').draggable({axis: 'x', containment:"parent", drag: function(event, ui) {
      var pos = self.$el.find('.person__progress__count__img').position();
      var countDel = self.$el.find('.person__progress').width();
      countDel = Math.floor(countDel / 10);
      self.$el.find('.person__progress__count').css({'width': pos.left + 20});
      /*if(pos.left % 10 === 0) {
        //console.log(posOld, pos.left);
          //posOld = pos.left;

      if(pos.left >= posOld && pos.left > 0) {
          ++z;
        //console.log(posOld);
        } else {
          --z;
        }
        if(z <= 10 && z >= 0) {
          self.$el.find('.person__progress__count__img h2').html(z);
        }
        posOld = pos.left;
      }*/
      z = Math.floor(pos.left / countDel) + 1;

      if(z <= 10 && z >= 1) {
          self.$el.find('.person__progress__count__img h2').html(z);
        }
    }});
  },
  heightMore: function() {
    this.$el.find('textarea').css({height: 250});
    return false;
  },
  heightDown: function() {
    //console.log('Event');
    this.$el.find('textarea').css({height: 46});
  }
});
