var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var Marionette = Backbone.Marionette = require('backbone.marionette');
var Collection = require('./../collection/RegCollection');

var RegItemView = Backbone.Marionette.ItemView.extend({
  modelEvens: {
    'change': 'render'
  },
  className: 'random-person__photo cursor',
  initialize: function() {
    //console.log('RegItemView init');
  },
  ui: {
    'btn': '.cl'
  },
  events: {
    'click @ui.btn': 'sel'
  },
  sel: function() {
    window.baseRoute.navigate('#person/' + this.model.get('screenName'), {trigger: true});
  },
  onRender: function() {
    $('.user__list').addClass('user-list__reg');
    var self = this;
    this.$el.click(function() {
      window.baseRoute.navigate('#person/' + self.model.get('screenName'), {trigger: true});
    });
    var stats = this.model.get('stats');
    if(stats !== undefined && stats.reputation !== 0 ) {
      this.$el.find('.random-person__bar').css({'width': stats.reputation + '%'});
    }
  },
  template: require('./template/reg_item--template.html')
});

module.exports = Backbone.Marionette.CompositeView.extend({
  className: 'row-full',
  initialize: function() {
    //console.log('RegCompositeView init');
    $('.user-list').addClass('align-str');
  },
  ui: {
    'search_btn': '.input__search__icon'
  },
  events: {
    'click @ui.search_btn': 'search'
  },
  search: function() {
    var tc = this.$el.find('input[type="text"]').val();
    var req = /https/gi;
    if(req.test(tc)) tc = tc.slice(15);
    window.baseRoute.navigate('#person/' + tc, {trigger: true});
  },
  template: require('./template/reg--template.html'),
  childViewContainer: "#random-container",
  collection: new Collection(),
  childView: RegItemView,
  onRender: function() {
    this.collection.fetch();
  }

});
