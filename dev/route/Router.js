var $ = window.jQuery = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
var Marionette = Backbone.Marionette = require('backbone.marionette');


module.exports = Backbone.Marionette.AppRouter.extend({
  appRoutes: {
    "": "reg",
    "abouttop": "aboutTop",
    "ptop": "personTop",
    "person/:id": "person",
    "person/:id/friends": "personFreand",
    "aboutlast": "aboutLast",
    "publish": "publish",
    "contacts": "contact",
    "about": "project",
    "emailConfirm?regToken=:id": "confirm",
    "passRese?resetToken=:id": "resetPass",
    "rule": "rule"
  }
});
